import com.google.gson.annotations.SerializedName;

public class Response {
	@SerializedName("VERSION")
	String version;
	@SerializedName("STATUS")
	String status;
	public String getVersion() {
		return version;
	}
	public void setVersion(String setVersion) {
		this.version = setVersion;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

	
}

