import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.jmx.AppenderDynamicMBean;

import com.google.gson.Gson;

public class JsonParsing {

	public static void main(String[] args) {
		JsonResponse jsonResponse =new JsonResponse();
		//payload
		
		Payload payload=new Payload();
		
		Request request=new Request();
		request.setProcessNm("GetSalesHierarchy");
		
		Jwt jwt=new Jwt();
		jwt.setRole("PWCM");
		
		Response response =new Response();
		response.setVersion("0.01");
		response.setStatus("SUCCESS");
		
		
		payload.setRequest(request);
		payload.setJwt(jwt);
		payload.setResponse(response);
		
       List<Payload> payloadList= new LinkedList<>();
		
		payloadList.add(payload);
	   jsonResponse.setPayLoad(payload);
	   Gson gson = new Gson();
		
		//defaultmap
		
		DefaultMap defaultMap =new DefaultMap();
		defaultMap.setFuwid("740");
		defaultMap.setDebug("INSIDE processTransactions 1");		
		
		List<DefaultMap> defaultList= new LinkedList<>();
		defaultList.add(defaultMap);
		jsonResponse.setDefaultMap(defaultMap);
		
		//ObjectMap
		
		ObjectMap objectMap=new ObjectMap();
		Evtkey evtkey=new Evtkey();
		evtkey.setEvtkey1("null");		
		evtkey.setEvtkey2("null");
		evtkey.setEvtkey3("null");
		evtkey.setEvt_ket_date("2017-03-21");
		evtkey.setEvt_key_time("19.28.56");
		
		//Jwt jwt1=new Jwt();
		
		objectMap.setEvtkey(evtkey);
		//objectMap.setJwt(jwt);
		List<ObjectMap> objectMapList=new LinkedList<>();
		
		objectMapList.add(objectMap);
		jsonResponse.setObjectMap(objectMap);
		
		//AlternateMap
		
		AlternateMap alternateMap=new AlternateMap();
		AmptHdr amptHdr=new AmptHdr();
		amptHdr.setProj_id("7");
		amptHdr.setEvent_type_id("740");
		amptHdr.setEvent_action("RAW");
		amptHdr.setParsed("true");
		amptHdr.setThread_sequence("50");
		
		
		MessageStatus messageStatus=new MessageStatus();		
		messageStatus.setmessageStatus("GO");
		
		alternateMap.setAmpthdr(amptHdr);
		alternateMap.setMessageStatus(messageStatus);
		List<AlternateMap> alternateMapList=new LinkedList<>();
		alternateMapList.add(alternateMap);
		jsonResponse.setAlternateMap(alternateMap);
		
	
		
		
		System.out.println(gson.toJson(jsonResponse));
	
		
		
		

	}

}
