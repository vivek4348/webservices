import com.google.gson.annotations.SerializedName;

public class Jwt {
	
	String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Jwt [role=" + role + "]";
	}

}

