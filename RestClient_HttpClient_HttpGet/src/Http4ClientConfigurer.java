


import java.security.KeyStore;
import java.util.ArrayList;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.camel.component.http4.HttpClientConfigurer;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Logger;

//import com.qwest.qxgf.infrastructure.util.TextUtil;

/*
 *  For Camel 2.13.0 and above only.  Camel versions below 2.13 require
 *  a different configureHttpClient method.
 */
public class Http4ClientConfigurer implements HttpClientConfigurer {
		
		private static Logger logger = Logger.getLogger(Http4ClientConfigurer.class);
		
		public static ArrayList<PoolingHttpClientConnectionManager> connManList = new ArrayList<PoolingHttpClientConnectionManager>();

		private boolean enablePreemptiveBasicAuthentication = false;	// Needs to be set to true when doing basic auth either dynamically or as a configuration
		private String httpBasicAuthUsername;	// Converted from httpBasicAuthCeredntials[0] so this could be configured with dynamic properties in CamelContext.xml
		private String httpBasicAuthPassword;	// Converted from httpBasicAuthCeredntials[1] so this could be configured with dynamic properties in CamelContext.xml
		private String protocolAlgorithm;
		private int maxConnectionsPerRoute; //maximum number of concurrent connections per route
		private int httpConnectionTimeout;		
		private int httpSocketTimeout;
		private int maxTotal;               //maximum number of total open connections
		private int connectionManagerTimeout;
		private int sessionTimeout;
		private int keepaliveTimeout = 1800000;
		private int port;								// required for sslProtocolSocketFactoryInfo
		private boolean staleCheckingEnabled = false;


		


		private String[] sslProtocolSocketFactoryInfo; 	// kestore file, keystore password, 
														// truststore file, truststore password, 
														// verifyHostname true/false
		
		
		
		public void configureHttpClient(HttpClientBuilder clientBuilder) {

			ConnectionKeepAliveStrategy myStrategy = new ConnectionKeepAliveStrategy() {

			    public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
			        // Honor 'keep-alive' header
			        HeaderElementIterator it = new BasicHeaderElementIterator(
			                response.headerIterator(HTTP.CONN_KEEP_ALIVE));
			        while (it.hasNext()) {
			            HeaderElement he = it.nextElement();
			            String param = he.getName();
			            String value = he.getValue();
			            if (value != null && param.equalsIgnoreCase("timeout")) {
			                try {
			                    return Long.parseLong(value) * 1000;
			                } catch(NumberFormatException ignore) {
			                }
			            }
			        }
			        return keepaliveTimeout;
			    }

			};
			
			clientBuilder.setKeepAliveStrategy(myStrategy);
			
			// set basic auth credentials if present
			try {
				if (httpBasicAuthUsername != null && httpBasicAuthPassword != null) {
					
					CredentialsProvider provider = new BasicCredentialsProvider();
					
					// Create the authentication scope
					AuthScope scope = new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM);
					
					// Create credential pair
					UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(httpBasicAuthUsername, httpBasicAuthPassword);
					
					// Inject the credentials
					provider.setCredentials(scope, credentials);
					clientBuilder.setDefaultCredentialsProvider(provider);
					logger.info("configuring HTTP basic auth credentials for username " + httpBasicAuthUsername);
					
				} else {
					logger.info("HTTP basic auth username & password not provided, not using basic auth in HTTP client configurer.");
				}
			} catch (NullPointerException npe) {
				logger.info("NullPointerException trying to set up HTTP basic auth credentials, not using basic auth in HTTP client configurer.");
			} catch (Exception e) {
				logger.error("Error configuring basic auth credentials\n" +e.getMessage());
			}
			
			// Set configured timeout information.
			RequestConfig.Builder requestBuilder = RequestConfig.custom();
			
			try {
				
				if (connectionManagerTimeout > 0) {
					logger.info("Setting connectionManagerTimeout to " + connectionManagerTimeout);
					requestBuilder = requestBuilder.setConnectionRequestTimeout(connectionManagerTimeout);
				} else {
					logger.debug("connectionManagerTimeout not provided, not being set.");
				}
			} catch (Exception e) {
				logger.warn("Error configuring HTTP connection manager timeout\n" +e.getMessage());
			}
			
			try {
				
				if (httpConnectionTimeout > 0) {
					logger.info("Setting http.connection.timeout to " + httpConnectionTimeout);
					requestBuilder = requestBuilder.setConnectTimeout(httpConnectionTimeout);
				} else {
					logger.debug("http.connection.timeout not provided, not being set.");
				}
			} catch (NullPointerException npe) {
				logger.info("HTTP connection timeout not provided, not being set.");
			} catch (Exception e) {
				logger.error("Error configuring HTTP connection timeout\n" +e.getMessage());
			}
			
			try {
				if (httpSocketTimeout > 0) {
					logger.info("Setting http.socket.timeout to " + httpSocketTimeout);
					requestBuilder = requestBuilder.setSocketTimeout(httpSocketTimeout);
				} else {
					logger.debug("http.socket.timeout not provided, not being set.");
				}
			} catch (NullPointerException npe) {
				logger.info("HTTP socket timeout not provided, not being set.");
			} catch (Exception e) {
				logger.error("Error configuring HTTP socket timeout\n" +e.getMessage());
			}
			
			if(isStaleCheckingEnabled()){
				logger.info("isStaleConnectionCheckEnabled ?:" + staleCheckingEnabled);
			   requestBuilder = requestBuilder.setStaleConnectionCheckEnabled(true);
			}else{
				logger.info("isStaleConnectionCheckEnabled ?:" + staleCheckingEnabled);
				requestBuilder = requestBuilder.setStaleConnectionCheckEnabled(false);
			}
			clientBuilder.setDefaultRequestConfig(requestBuilder.build());
			
			
			/*
			 * Register https scheme in the connection manager
			 * The JKS file should be in your classpath. 
			 * The parameters for getResource are:
			 * keystore file
			 * keystore file password
			 * truststore file
			 * truststore file password
			 * verifyHostname true/false
			 * 
			 * keystore file and truststore file can be the same file.  The file must contain a private key, even
			 * if it is not required.  You can use the ecomtester cert for the private key if you don't already have one.
			 */
			
			try {
				if (sslProtocolSocketFactoryInfo != null 
						&& (sslProtocolSocketFactoryInfo.length == 3 
								|| sslProtocolSocketFactoryInfo.length == 5)
								&& sslProtocolSocketFactoryInfo[sslProtocolSocketFactoryInfo.length -1].matches("true|false")
								&& port > 0) {

					boolean verifyHostname = Boolean.parseBoolean(
							sslProtocolSocketFactoryInfo[sslProtocolSocketFactoryInfo.length -1]);
					
					String keyStoreFile = sslProtocolSocketFactoryInfo[0];
					String keyStorePassword = sslProtocolSocketFactoryInfo[1];
					String trustStoreFile = null;
					String trustStorePassword = null;

					if (sslProtocolSocketFactoryInfo.length == 3) {
						trustStoreFile = sslProtocolSocketFactoryInfo[0];
						trustStorePassword = sslProtocolSocketFactoryInfo[1];
					} else {
						trustStoreFile = sslProtocolSocketFactoryInfo[2];
						trustStorePassword = sslProtocolSocketFactoryInfo[3];
					}
					
					X509HostnameVerifier hostnameVerifier = SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
					if (verifyHostname) {	
						hostnameVerifier = SSLConnectionSocketFactory.STRICT_HOSTNAME_VERIFIER;
					}					
					
					KeyStore keystore = KeyStore.getInstance("JKS");
					KeyStore truststore = KeyStore.getInstance("JKS");
					keystore.load(TextUtil.getFileFromClasspathAsStream(keyStoreFile), keyStorePassword.toCharArray());
					truststore.load(TextUtil.getFileFromClasspathAsStream(trustStoreFile), trustStorePassword.toCharArray());
					TrustManagerFactory trustFactory = TrustManagerFactory.getInstance("SunX509");
				    trustFactory.init(truststore);
				    KeyManagerFactory keyFactory = KeyManagerFactory.getInstance("SunX509");
				    keyFactory.init(keystore, keyStorePassword.toCharArray());
				    
				    if (protocolAlgorithm == null || protocolAlgorithm.length() < 3) {
				    	protocolAlgorithm = "TLSv1";
				    }
				    logger.debug("protocolAlgorithm is " +protocolAlgorithm);
				 
				    SSLContext sslcontext = SSLContext.getInstance(protocolAlgorithm);
					sslcontext.init(keyFactory.getKeyManagers(), trustFactory.getTrustManagers(), null);
					
					String[] protocolArray = sslcontext.getSupportedSSLParameters().getProtocols();
					if (protocolAlgorithm.trim().equalsIgnoreCase("TLSv1.2")) {
						logger.debug("setProtocol to TLSv1.2");
						protocolArray=new String[]{"TLSv1.2"};
					}
					else if (protocolAlgorithm.trim().equalsIgnoreCase("TLSv1.1")) {
						logger.debug("setProtocol to TLSv1.1");
						protocolArray=new String[]{"TLSv1.1"};
					}
					else {
						logger.debug("setProtocol to TLSv1");
						protocolArray=new String[]{"TLSv1"};
					}
					sslcontext.getSupportedSSLParameters().setProtocols(protocolArray);

					if (sessionTimeout > 0) {
						sslcontext.getClientSessionContext().setSessionTimeout(sessionTimeout);
					}

					String[] cipherSuites = sslcontext.getSupportedSSLParameters().getCipherSuites();
					SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
							sslcontext, protocolArray, cipherSuites, hostnameVerifier);

					
					Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
							.register("https4", sslsf)
							.register("https", sslsf)
					        .build();

					PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(registry);

					if (maxConnectionsPerRoute > 0) {
						logger.info("Setting max connections per route to " + maxConnectionsPerRoute);
						connManager.setDefaultMaxPerRoute(maxConnectionsPerRoute);
					} else {
						connManager.setDefaultMaxPerRoute(3);
						logger.debug("Max connections per route not provided, setting to a default of 3.");
					}

					if (maxTotal > 0) {
						logger.info("Setting maximum number of total open connections " + maxTotal);
						connManager.setMaxTotal(maxTotal);
					} else {
							logger.debug("Total open connections not provided, default is 20 connections in total.");
						} 
					
					clientBuilder.setConnectionManager(connManager);
					
					//Adding connection manager to List to cleanup connections.
					if (!connManList.contains(connManager)){
						logger.info("Adding  connManager to List:"+connManager);
						connManList.add(connManager);
					}
					
					
					logger.info("Registered https scheme using "+ keyStoreFile);
					
				} else {
					logger.info("sslProtocolSocketFactoryInfo not provided, skipping SSL configuration");
				}
			} catch(Exception e) {
				logger.error("Error while registering https scheme using " + e.getMessage());
			}
			
			clientBuilder.build();
		}
		

		public int getHttpConnectionTimeout() {
			return httpConnectionTimeout;
		}


		public void setHttpConnectionTimeout(int httpConnectionTimeout) {
			this.httpConnectionTimeout = httpConnectionTimeout;
		}


		public int getHttpSocketTimeout() {
			return httpSocketTimeout;
		}


		public void setHttpSocketTimeout(int httpSocketTimeout) {
			this.httpSocketTimeout = httpSocketTimeout;
		}
		
		public int getConnectionManagerTimeout() {
			return connectionManagerTimeout;
		}

		
		public void setConnectionManagerTimeout(int connectionManagerTimeout) {
			this.connectionManagerTimeout = connectionManagerTimeout;
		}
		
		public int getSessionTimeout() {
			return sessionTimeout;
		}

		public void setSessionTimeout(int sessionTimeout) {
			this.sessionTimeout = sessionTimeout;
		}


		public String[] getSslProtocolSocketFactoryInfo() {
			return sslProtocolSocketFactoryInfo;
		}


		public void setSslProtocolSocketFactoryInfo(
				String[] sslProtocolSocketFactoryInfo) {
			this.sslProtocolSocketFactoryInfo = sslProtocolSocketFactoryInfo;
		}


		public int getPort() {
			return port;
		}


		public void setPort(int port) {
			this.port = port;
		}


		public boolean isEnablePreemptiveBasicAuthentication() {
			return enablePreemptiveBasicAuthentication;
		}


		public void setEnablePreemptiveBasicAuthentication(
				boolean enablePreemptiveBasicAuthentication) {
			this.enablePreemptiveBasicAuthentication = enablePreemptiveBasicAuthentication;
		}


		public String getHttpBasicAuthUsername() {
			return httpBasicAuthUsername;
		}


		public void setHttpBasicAuthUsername(String httpBasicAuthUsername) {
			this.httpBasicAuthUsername = httpBasicAuthUsername;
		}


		public String getHttpBasicAuthPassword() {
			return httpBasicAuthPassword;
		}


		public void setHttpBasicAuthPassword(String httpBasicAuthPassword) {
			this.httpBasicAuthPassword = httpBasicAuthPassword;
		}



		public String getProtocolAlgorithm() {
			return protocolAlgorithm;
		}



		public void setProtocolAlgorithm(String protocolAlgorithm) {
			this.protocolAlgorithm = protocolAlgorithm;
		}

		
		public int getKeepaliveTimeout() {
			return keepaliveTimeout;
		}



		public void setKeepaliveTimeout(int keepaliveTimeout) {
			this.keepaliveTimeout = keepaliveTimeout;
		}
		
		public int getMaxConnectionsPerRoute() {
			return maxConnectionsPerRoute;
		}


		public void setMaxConnectionsPerRoute(int maxConnectionsPerRoute) {
			this.maxConnectionsPerRoute = maxConnectionsPerRoute;
		}

		/**
		 * @return the staleCheckingEnabled
		 */
		public boolean isStaleCheckingEnabled() {
			return staleCheckingEnabled;
		}


		/**
		 * @param staleCheckingEnabled the staleCheckingEnabled to set
		 */
		public void setStaleCheckingEnabled(boolean staleCheckingEnabled) {
			this.staleCheckingEnabled = staleCheckingEnabled;
		}
		
		/**
		 * @return the maxTotal
		 */
		public int getMaxTotal() {
			return maxTotal;
		}


		/**
		 * @param maxTotal the maxTotal to set
		 */
		public void setMaxTotal(int maxTotal) {
			this.maxTotal = maxTotal;
		}


		@Override
		public void configureHttpClient(HttpClient arg0) {
			// TODO Auto-generated method stub
			
		}


}
