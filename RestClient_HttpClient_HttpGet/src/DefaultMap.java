import com.google.gson.annotations.SerializedName;

public class DefaultMap {
	
	@SerializedName("FUW_ID")
	String fuwid;
	
	@SerializedName("DEBUG")
	String debug;

	public String getFuwid() {
		return fuwid;
	}

	public void setFuwid(String fuwid) {
		this.fuwid = fuwid;
	}

	public String getDebug() {
		return debug;
	}

	public void setDebug(String debug) {
		this.debug = debug;
	}

	@Override
	public String toString() {
		return "DefaultMap [fuwid=" + fuwid + ", debug=" + debug + "]";
	}
	
	
	
	

}

