import java.sql.Date;
import java.sql.Time;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class ObjectMap {
	
	@SerializedName("EVTKEY")
	private Evtkey  evtkey = new Evtkey();

	public Evtkey getEvtkey() {
		return evtkey;
	}

	public void setEvtkey(Evtkey evtkey) {
		this.evtkey = evtkey;
	}
	
	@SerializedName("JWT")
	private Jwt  jwt1 = new Jwt();

	public Jwt getJwt() {
		return jwt1;
	}

	public void setJwt(Jwt jwt) {
		this.jwt1 = jwt;
	}

}

