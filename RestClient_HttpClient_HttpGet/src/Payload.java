import com.google.gson.annotations.SerializedName;

public class Payload {
	
	@SerializedName("REQUEST")
	private Request  request = new Request();
	
	@SerializedName("JWT")
	private Jwt  jwt = new Jwt();
	
	@SerializedName("RESPONSE")
	private Response  response = new Response();
	
	
	

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public Jwt getJwt() {
		return jwt;
	}

	public void setJwt(Jwt jwt) {
		this.jwt = jwt;
	}
	
	
	
	

}
