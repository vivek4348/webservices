/*
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


import java.io.File;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
/*import org.apache.http.conn.ssl.NoopHostnameVerifier;*/
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
/*import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;*/
import org.apache.log4j.Logger;
import org.apache.sling.commons.json.JSONObject;


public class TestMainPreemptive {
	
	final static Logger logger = Logger.getLogger(TestMainPreemptive.class);
	private static final String SALES_ID = "SALESID";
	private static final String ITEMS = "ITEMS";
	private static final String RESPONSE_TYPE_JSON = "application/json;charset=UTF-8";
	private static final String PAYLOAD = "payLoad";

    public final static void main(String[] args) throws Exception {
      
    	String json = getTextSearchResults("pwcm", "https://cxg7o.test.intranet/E/N/052AMP/ServicesDEV");
    	System.out.println(json);
    }
    
    private static String getTextSearchResults(String salesId, String hmWebServiceURL) {
		String json ="";
		if(!StringUtils.isBlank(salesId) && !StringUtils.isBlank(hmWebServiceURL)){
						
					
			JSONObject jsonObjectMain = new JSONObject();
			try {
				JSONObject requestJsonObject =new JSONObject("{\"REQUEST\":{\"PROCESS_NM\":\"GetSalesHierarchy\"},\"JWT\":{\"role\":\"PWCM\"}}");
				
				JSONObject salesIdJsonObject = new JSONObject();
				salesIdJsonObject.put(SALES_ID, salesId);				
				
				JSONObject salesIDJsonObjectfinal = new JSONObject();
				salesIDJsonObjectfinal.put("0", salesIdJsonObject);	
				requestJsonObject.accumulate(ITEMS, salesIDJsonObjectfinal);
				
				jsonObjectMain.accumulate(PAYLOAD, requestJsonObject);
				
				String jksFile = "C:\\Users\\ab79287\\Desktop\\Cert\\pwcm-cxg-test2.jks";
				String jksFilePassword = "!Centurylink700";
				String username = "ESLCXG7";
		        String password = "oVyFR3E5s1AN56pIMqNH!";
		        
		        
				
				File file = new File(jksFile);
		        SSLContext sslcontext = null ; /*SSLContexts.custom()
		                .loadTrustMaterial(file, jksFilePassword.toCharArray(),
		                		new TrustStrategy() {                   	
							public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {						
								return true;
							}
		                })
		                .build();*/
		        
		        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1", "TLSv1.1", "TLSv1.2" }, null, null /*NoopHostnameVerifier.INSTANCE*/);        
		        
		        HttpHost target = new HttpHost("cxg7o.test.intranet", 443, "https");
		        CredentialsProvider provider = new BasicCredentialsProvider();
				UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
				provider.setCredentials(new AuthScope(target.getHostName(), target.getPort()),
						credentials);
				
				
	            AuthCache authCache = new BasicAuthCache();
	            BasicScheme basicAuth = new BasicScheme();
	            authCache.put(target, basicAuth);            
	            HttpClientContext localContext = HttpClientContext.create();
	            localContext.setAuthCache(authCache);
	            
				CloseableHttpClient httpClient = HttpClientBuilder.create().setSSLSocketFactory(sslsf)
						.setDefaultCredentialsProvider(provider)
						.build();
				HttpPost httpPost = new HttpPost(hmWebServiceURL);
				String requestString = "{\"payLoad\":{\"REQUEST\":{\"PROCESS_NM\":\"GetSalesHierarchy\"},\"ITEMS\":{\"0\":{\"SALES_ID\":\"PWCM\"}}}}";
                StringEntity entity = new StringEntity(requestString);
                entity.setContentType(RESPONSE_TYPE_JSON);
                httpPost.setEntity(entity);
                
                 
                HttpResponse  response = httpClient.execute(target,httpPost,localContext);
                System.out.println("Test 4 First Response: " + String.valueOf(response));
                
                /*byte[] credentialsByte = Base64.encodeBase64((username + ":" + password).getBytes(StandardCharsets.UTF_8));
                httpPost.setHeader("Authorization", "Basic " + new String(credentialsByte, StandardCharsets.UTF_8));
                response = httpClient.execute(httpPost);
                System.out.println("Test 4 Second Response: " + String.valueOf(response));
				if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					System.out.println("Error While Calling Web hmWebServiceURL: " + hmWebServiceURL + " HTTP error code : " + response.getStatusLine().getStatusCode());
        			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
        		}else{        			
        			json = EntityUtils.toString(response.getEntity());
        			logger.debug("json: " + json);       			      			
        		}*/
				
			} catch (Exception e) {				
				System.out.println("Exception While Calling Web hmWebServiceURL: " + hmWebServiceURL + ", Exception: " + ExceptionUtils.getStackTrace(e));
			}
		}
		return json;
	}
	
}