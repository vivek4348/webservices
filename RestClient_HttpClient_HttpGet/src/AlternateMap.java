import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class AlternateMap {

	
	@SerializedName("AMPT_HDR")
	private AmptHdr  ampthdr = new AmptHdr();
	

	@SerializedName("MESSAGE_STATUS")
	private MessageStatus  messageStatus = new MessageStatus();
	

	public AmptHdr getAmpthdr() {
		return ampthdr;
	}

	public void setAmpthdr(AmptHdr ampthdr) {
		this.ampthdr = ampthdr;
	}

	public MessageStatus getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(MessageStatus messageStatus) {
		this.messageStatus = messageStatus;
	}
	
	
	
	
}


