import com.google.gson.annotations.SerializedName;

public class JsonResponse {
	
	@SerializedName("payLoad")
	private Payload payLoad = new Payload();
	
	@SerializedName("defaultMap")
	private DefaultMap defaultMap = new DefaultMap();
	
	@SerializedName("objectMap")
	private ObjectMap objectMap = new ObjectMap();
	
	@SerializedName("alternateMap")
	private AlternateMap alternateMap = new AlternateMap();

	public Payload getPayLoad() {
		return payLoad;
	}

	public void setPayLoad(Payload payLoad) {
		this.payLoad = payLoad;
	}

	public DefaultMap getDefaultMap() {
		return defaultMap;
	}

	public void setDefaultMap(DefaultMap defaultMap) {
		this.defaultMap = defaultMap;
	}

	public ObjectMap getObjectMap() {
		return objectMap;
	}

	public void setObjectMap(ObjectMap objectMap) {
		this.objectMap = objectMap;
	}

	public AlternateMap getAlternateMap() {
		return alternateMap;
	}

	public void setAlternateMap(AlternateMap alternateMap) {
		this.alternateMap = alternateMap;
	}
	
	
}
 