import com.google.gson.annotations.SerializedName;

public class MessageStatus {
	@SerializedName("MESSAGE_STATUS")
	String messageStatus;

	public String getmessageStatus() {
		return messageStatus;
	}

	public void setmessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	@Override
	public String toString() {
		return "MessageStatus [messageStatus=" + messageStatus + "]";
	}

	
}

