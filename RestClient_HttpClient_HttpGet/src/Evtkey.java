import java.sql.Date;
import java.sql.Time;

import com.google.gson.annotations.SerializedName;

public class Evtkey {
	@SerializedName("EVTKEY1")
	String evtkey1;
	@SerializedName("EVTKEY2")
	String evtkey2;
	@SerializedName("EVTKEY3")
	String evtkey3;
	@SerializedName("EVT_KEY_DATE")
	String evt_ket_date;
	@SerializedName("EVT_KEY_TIME")
	String evt_key_time;
	public String getEvtkey1() {
		return evtkey1;
	}
	public void setEvtkey1(String evtkey1) {
		this.evtkey1 = evtkey1;
	}
	public String getEvtkey2() {
		return evtkey2;
	}
	public void setEvtkey2(String evtkey2) {
		this.evtkey2 = evtkey2;
	}
	public String getEvtkey3() {
		return evtkey3;
	}
	public void setEvtkey3(String evtkey3) {
		this.evtkey3 = evtkey3;
	}
	public String getEvt_ket_date() {
		return evt_ket_date;
	}
	public void setEvt_ket_date(String evt_ket_date) {
		this.evt_ket_date = evt_ket_date;
	}
	public String getEvt_key_time() {
		return evt_key_time;
	}
	public void setEvt_key_time(String evt_key_time) {
		this.evt_key_time = evt_key_time;
	}


}
