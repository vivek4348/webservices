import com.google.gson.annotations.SerializedName;

public class AmptHdr {
	
	   @SerializedName("PROJ_ID")
	   String proj_id;
	   @SerializedName("EVENT_TYP_ID")
	   String event_type_id;
	   @SerializedName("EVENT_ACTION")
	   String event_action;
	   @SerializedName("PARSED")
	   String parsed;
	   @SerializedName("THREAD_SEQUENCE")
	   String thread_sequence;
	public String getProj_id() {
		return proj_id;
	}
	public void setProj_id(String proj_id) {
		this.proj_id = proj_id;
	}
	public String getEvent_type_id() {
		return event_type_id;
	}
	public void setEvent_type_id(String event_type_id) {
		this.event_type_id = event_type_id;
	}
	public String getEvent_action() {
		return event_action;
	}
	public void setEvent_action(String event_action) {
		this.event_action = event_action;
	}
	public String getParsed() {
		return parsed;
	}
	public void setParsed(String parsed) {
		this.parsed = parsed;
	}
	public String getThread_sequence() {
		return thread_sequence;
	}
	public void setThread_sequence(String thread_sequence) {
		this.thread_sequence = thread_sequence;
	}

}
