
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Test2WaySSL2 {

	public static void main(String[] args) throws ClientProtocolException, IOException, KeyManagementException,
			NoSuchAlgorithmException, KeyStoreException {

		/*CloseableHttpClient httpclient = createApacheHttp4ClientWithClientCertAuth();
		String url = "https://cxg7o.test.intranet/E/N/052AMP/ServicesDEV";
		String data = "{\"payLoad\":{\"REQUEST\":{\"PROCESS_NM\":\"GetSalesHierarchy\"},\"ITEMS\":{\"0\":{\"SALES_ID\":\"PWCM\"}}}}";
		String username = "PWCMCxg7";
		String password = "2pUyhs8xzwN5Z0y4tgP5!";

		HttpPost request = new HttpPost(url);
		StringEntity params = new StringEntity(data.toString());

		byte[] credentialsByte = Base64.encodeBase64((username + ":" + password).getBytes(StandardCharsets.UTF_8));
		request.setHeader("Authorization", "Basic " + new String(credentialsByte, StandardCharsets.UTF_8));
		request.addHeader("content-type", "application/json");
		request.addHeader("Accept", "application/json");
		request.setEntity(params);
		CloseableHttpResponse response = httpclient.execute(request);
		String jsonResponseFinal=EntityUtils.toString(response.getEntity());		
		System.out.println("HMService Response: " + jsonResponseFinal );*/
		
		/*//String to BufferedReader
		InputStream is = new ByteArrayInputStream(jsonResponseFinal.getBytes());
		
		BufferedReader br = new BufferedReader(new InputStreamReader(is));*/
	     Gson gson = new Gson();
		
		//Map<String, JsonResponse> map = gson.fromJson( jsonResponseFinal, new TypeToken<Map<String, JsonResponse>>(){}.getType());
		 
	     /*JsonResponse jsonResponse = gson.fromJson( jsonResponseFinal, JsonResponse.class);   
         System.out.println(jsonResponse);*/
	     
	 
	     

		
		
		
	}
	public static CloseableHttpClient createApacheHttp4ClientWithClientCertAuth() {
		try {
			String jksFile = "C:\\Users\\ab79287\\Desktop\\Cert\\pwcm-cxg-test2.jks";
			String jksFilePassword = "!Centurylink700";

			KeyStore keyStore = KeyStore.getInstance("jks");
			keyStore.load(new FileInputStream(jksFile), jksFilePassword.toCharArray());

			SSLContext sslContext = SSLContexts.custom().loadKeyMaterial(keyStore, jksFilePassword.toCharArray())
					.build();
			SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(sslContext, new AllowAllHostnameVerifier());
			return HttpClients.custom().setSSLSocketFactory(sslConnectionFactory).build();
					} 
		    catch (Exception ex) {

			ex.printStackTrace();
			return null;
		}
	}

}

/* final JSONObject obj = new JSONObject(jsonResponseFinal);
final JSONArray geodata = obj.getJSONArray("geodata");
final int n = geodata.length();
for (int i = 0; i < n; ++i) {
  final JSONObject person = geodata.getJSONObject(i);
  System.out.println(person.getInt("id"));
  System.out.println(person.getString("name"));
  System.out.println(person.getString("gender"));
  System.out.println(person.getDouble("latitude"));
  System.out.println(person.getDouble("longitude"));
}
*/
