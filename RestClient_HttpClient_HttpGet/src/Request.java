import com.google.gson.annotations.SerializedName;

public class Request {
	
	@SerializedName("PROCESS_NM")
	String processNm;

	public String getProcessNm() {
		return processNm;
	}

	public void setProcessNm(String processNm) {
		this.processNm = processNm;
	}

	
}

