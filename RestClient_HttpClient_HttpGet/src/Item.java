public class Item {
	
	String salesHierarchy;

	public String getSalesHierarchy() {
		return salesHierarchy;
	}

	public void setSalesHierarchy(String salesHierarchy) {
		this.salesHierarchy = salesHierarchy;
	}

	@Override
	public String toString() {
		return "Items [salesHierarchy=" + salesHierarchy + "]";
	}
	



}

