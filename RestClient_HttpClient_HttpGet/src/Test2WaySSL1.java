

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class Test2WaySSL1 {

	public static void main(String[] args) throws ClientProtocolException, IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

		CloseableHttpClient httpclient = createApacheHttp4ClientWithClientCertAuth();
			    
		String url = "https://cxg7o.test.intranet/E/N/052AMP/ServicesDEV";
		String data = "{\"payLoad\":{\"REQUEST\":{\"PROCESS_NM\":\"GetSalesHierarchy\"},\"ITEMS\":{\"0\":{\"SALES_ID\":\"PWCM\"}}}}";
		
		
		String username="PWCMCxg7";
		String password="2pUyhs8xzwN5Z0y4tgP5!";

		HttpPost request = new HttpPost(url);
		StringEntity params = new StringEntity(data.toString());
		
		byte[] credentialsByte = Base64.encodeBase64((username + ":" + password).getBytes(StandardCharsets.UTF_8));	
		request.setHeader("Authorization", "Basic " + new String(credentialsByte, StandardCharsets.UTF_8));
		request.addHeader("content-type", "application/json");
		request.addHeader("Accept", "application/json");
		request.setEntity(params);
		CloseableHttpResponse response = httpclient.execute(request);
		
		System.out.println("pwcm: " + EntityUtils.toString(response.getEntity()));

//		HttpEntity entity = response.getEntity();
//		if (entity != null) {
//			System.out.println("Response content length: " + entity.getContentLength());
//		}
//		EntityUtils.consume(entity);
	}
	
	public static CloseableHttpClient createApacheHttp4ClientWithClientCertAuth() {
	    try {
	    	String jksFile = "C:\\Users\\ab79287\\Desktop\\Cert\\pwcm-cxg-test2.jks";
			String jksFilePassword = "!Centurylink700";
	        /*SSLContext sslContext = SSLContexts
	                .custom()
	                .loadKeyMaterial(new File(jksFile), jksFilePassword.toCharArray(), new TrustStrategy() {                   	
						public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {						
							return true;
						}})
	                .build(); 

	        SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(sslContext,
	        		NoopHostnameVerifier.INSTANCE);

	        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
	                .register("https", sslConnectionFactory)
	                .register("http", new PlainConnectionSocketFactory())
	                .build();

	        HttpClientBuilder builder = HttpClientBuilder.create();
	        builder.setSSLSocketFactory(sslConnectionFactory);
	        builder.setConnectionManager(new PoolingHttpClientConnectionManager(registry));*/
			KeyStore keyStore = KeyStore.getInstance("jks");
			keyStore.load(new FileInputStream(jksFile), jksFilePassword.toCharArray());

			
			
			
			SSLContext sslContext = SSLContexts.custom().loadKeyMaterial(keyStore, jksFilePassword.toCharArray())
					.build();
			SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(sslContext, new AllowAllHostnameVerifier());
			return HttpClients.custom().setSSLSocketFactory(sslConnectionFactory).build();
			
			


	        //return builder.build();
	    } catch (Exception ex) {

	        ex.printStackTrace();
	        return null;
	    }
	}

}
